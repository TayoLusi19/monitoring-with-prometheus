Testing your alerting setup is crucial to ensure that your monitoring system is correctly configured and capable of notifying you about real incidents in your environment. Here are some ways to simulate issues in your Kubernetes cluster to trigger the alerts configured in the previous steps.

## 1. Triggering an Alert for the Java or MySQL Application (Slack Notification)
Delete a MySQL Pod
One of the alerts we configured was to notify us when a MySQL instance goes down. You can simulate this by manually deleting one of the MySQL pods if MySQL is deployed as a StatefulSet with multiple replicas.

```bash
kubectl delete pod -l app=mysql -n my-app --grace-period=0 --force
```

This action should trigger an alert because the number of available MySQL instances will temporarily be less than the desired state, assuming your alert condition checks for such a scenario.

**Access a Non-existent Path on the Java Application**
If you have configured an alert based on the error rate or specific HTTP status codes from your Java application, you can trigger this alert by attempting to access a path that doesn't exist, thereby generating 404 errors.

You might need to automate this to generate a significant number of errors quickly. Here’s a simple loop in bash that uses curl to do this:

```bash
for i in {1..100}; do curl http://your-java-app-ingress/path-that-doesnt-exist; done
```

## 2. Triggering an Alert for the Nginx Ingress or Kubernetes Components (Email Notification)
**Delete a StatefulSet Pod**
For alerts related to Kubernetes components or specifically for a StatefulSet replica mismatch, you can delete a pod managed by a StatefulSet. If MySQL is deployed as a StatefulSet, this action could trigger both the MySQL-related alert and the StatefulSet replica mismatch alert.

```bash
kubectl delete pod <statefulset-pod-name> -n my-app --grace-period=0 --force
```

Replace <statefulset-pod-name> with the actual name of one of the pods managed by your StatefulSet.

**Monitor Notifications**
- Slack: Check the designated Slack channel for incoming alerts related to the actions performed above. You should receive notifications about the issues you've simulated.

- Email: Check the email address configured in the Alertmanager setup for notifications. Depending on your email provider's settings and the configuration of the SMTP server, there might be a slight delay in receiving the email.

## Verifying Alert Recovery
After simulating the issues and confirming that alerts were triggered as expected, ensure that your system recovers and sends recovery notifications if you've configured Alertmanager to do so.

- For the MySQL pod or StatefulSet pod you deleted, Kubernetes should automatically recreate the pod to match the desired state of the deployment or StatefulSet.
- For the Java application, cease the generation of 404 errors and observe if the error rate alert recovers.

### Conclusion
Testing alerts by simulating real-world issues is a critical step in validating your monitoring and alerting setup. It ensures that you're well-prepared to handle incidents proactively. Always review the configuration and adjust thresholds and conditions based on the behavior observed during these tests to fine-tune your monitoring system.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/monitoring-with-prometheus/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
