# Deploy your Application and Prepare the Setup Project

Deploying your application with MySQL database and setting up monitoring with Prometheus in a Kubernetes environment is a comprehensive project that enhances the observability and reliability of your services. Here's a guide to help you prepare the setup as described in your context.

## Step 1: Create Kubernetes Cluster on LKE
- Use the Linode Kubernetes Engine (LKE) to create a new cluster. Configure the cluster with the desired version and number of nodes.

## Step 2: Configure Kubectl
After creating the cluster in LKE, download the kubeconfig file and set up your kubectl to connect to the new cluster:

```bash
chmod 400 ~/Downloads/monitoring-kubeconfig.yaml
export KUBECONFIG=~/Downloads/monitoring-kubeconfig.yaml
```

## Step 3: Create Docker Registry Secret
This secret is necessary for Kubernetes to pull private images from Docker Hub.

```bash
kubectl create secret docker-registry my-registry-key \
  --docker-server=docker.io \
  --docker-username=your_dockerID \
  --docker-password=your_dockerhub_pwd \
  --docker-email=your_dockerhub_email \
  -n my-app
```

## Step 4: Deploy MySQL Database and Java Application
You mentioned using an Ansible playbook for deployment, which indicates you're familiar with Ansible for automating deployment tasks.

MySQL Helm Chart:

Update your Ansible playbook to include a task that uses Helm to deploy MySQL with 2 replicas:

```bash
- name: Deploy MySQL Database
  community.kubernetes.helm:
    name: mysql
    chart_repo: https://charts.bitnami.com/bitnami
    chart_name: mysql
    chart_version: "<version>"
    release_namespace: my-app
    values:
      architecture: replication
      replicaCount: 2
```

Java Application Deployment:

Ensure your playbook includes tasks for deploying your Java Maven application with 3 replicas. You can configure this through Kubernetes Deployment and Service objects.

## Step 5: Deploy Nginx Ingress Controller
Use Helm to deploy the Nginx Ingress Controller:

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx --namespace my-app --create-namespace
```

## Step 6: Configure Ingress Rule
Define an Ingress rule to access your Java application. This can be part of your Ansible playbook or manually created as a YAML file.

```bash
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: java-app-ingress
  namespace: my-app
spec:
  rules:
  - host: java-app.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: java-app-service
            port:
              number: 80
```

## Step 7: Handling ValidationWebhookConfiguration Error
If you encounter issues with the Ingress component related to the nginx-controller-admission webhook, use the commands provided to delete the problematic webhook configuration and try deploying again.

Next Steps:
- After setting up your application, MySQL, and Ingress controller, the next step is to install and configure Prometheus for monitoring. Prometheus can be installed using the community Prometheus Helm chart.
- Configure Prometheus to scrape metrics from your application and MySQL database. You may need to enable and configure exporters for MySQL and instrument your Java application with Prometheus client libraries.

This setup ensures your application is deployed with high availability and is accessible through a domain name. With Prometheus, you'll gain insights into the performance and health of your application and database, enabling proactive issue resolution.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/monitoring-with-prometheus/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
