# Deploy your Application and Prepare the Setup Project

Deploying the Prometheus Operator in your Kubernetes cluster and configuring metrics scraping for Nginx, MySQL, and your Java application involves several steps. This guide will walk you through these processes, ensuring that you can monitor all three components effectively.

### Deploy Prometheus Operator
Add the Prometheus Helm Chart Repository and Update:

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

### Create a Namespace for Monitoring and Deploy Prometheus Operator:

```bash
kubectl create namespace monitoring
helm install monitoring-stack prometheus-community/kube-prometheus-stack -n monitoring
```

### Access Prometheus UI
Port Forward Prometheus Service to Access UI Locally:

```bash
kubectl port-forward svc/monitoring-stack-kube-prom-prometheus 9090:9090 -n monitoring
```

Open a browser and go to http://127.0.0.1:9090/targets to view Prometheus targets.

## Configure Metrics Scraping
Nginx Ingress Controller
The Nginx Ingress Controller, if deployed using the recommended Helm chart, has Prometheus metrics exposed by default. Ensure the ServiceMonitor or PodMonitor is correctly configured to scrape metrics.
MySQL
Uninstall Previous MySQL Release (if exists):

```bash
helm uninstall mysql-release
```

### Deploy MySQL with Metrics Enabled:

Use the Bitnami MySQL Helm chart and enable metrics in your Helm values.

### Java Application
- Expose Metrics from Java Application:

- Ensure your Java application exposes metrics on port 8081. You might need to include a Prometheus client in your application code.

- Build and Push the Java Application Docker Image:

- Update your Java application to include Prometheus metrics, build the Docker image, and push it to Docker Hub.

```bash
docker build -t {docker-hub-id}/{repo-name}:{tag} .
docker push {docker-hub-id}/{repo-name}:{tag}
```

### Update Kubernetes Manifest for Java Application:

In java-app.yaml, set the correct image name and ensure the application exposes metrics on the correct port.

Ansible Playbook for Metrics Scraping Configuration
Create an Ansible Playbook (2-configure-k8s.yaml): 

This playbook should configure ServiceMonitors or PodMonitors for Nginx, MySQL, and your Java application. Ensure labels match those expected by Prometheus for scraping.

Here's an example task that configures a ServiceMonitor for the Java application:

```bash
- name: Configure ServiceMonitor for Java Application
  k8s:
    state: present
    namespace: monitoring
    definition:
      apiVersion: monitoring.coreos.com/v1
      kind: ServiceMonitor
      metadata:
        name: java-app
        labels:
          release: monitoring-stack
      spec:
        selector:
          matchLabels:
            app: java-app
        endpoints:
        - port: http
          path: /actuator/prometheus
          interval: 30s
```

Adjust the matchLabels, port, and path according to your setup.

## Verify Monitoring Targets
After applying the configurations, revisit the Prometheus UI (http://127.0.0.1:9090/targets) to ensure new targets for MySQL, Nginx, and your Java application have been added and are up.
This setup provides comprehensive monitoring for your application stack, allowing you to proactively address issues and improve system reliability.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/monitoring-with-prometheus/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
