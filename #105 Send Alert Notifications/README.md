Configuring Alertmanager to send notifications to different destinations based on the alert's origin or label requires setting up routing rules in your Alertmanager configuration. For this exercise, we will configure Alertmanager to send notifications to a Slack channel for issues related to the Java or MySQL applications and to an email address for issues related to the Nginx Ingress Controller or Kubernetes components.

## Step 1: Set Up Slack Notifications
Create a Slack Workspace and Channel if you haven't already.
Create a Slack App:
Navigate to https://api.slack.com/apps and create a new app.
Choose "From scratch" and give your app a name and select a workspace.
Go to "Incoming Webhooks", activate incoming webhooks, and create a new webhook to the channel you've designated for alerts.
Copy the webhook URL; you will need this to configure Alertmanager.

## Step 2: Configure Email Notifications
Assuming you have an SMTP server available (you can use Gmail or another service):

Ensure you have the SMTP server details, including the host, port, username, and password.

## Step 3: Create Kubernetes Secrets for Slack and Email
Create a secret for Slack webhook URL and another for email credentials.

4-slack-secret.yaml

```bash
apiVersion: v1
kind: Secret
metadata:
  name: alertmanager-slack
  namespace: monitoring
type: Opaque
stringData:
  slack-webhook-url: 'YOUR_SLACK_WEBHOOK_URL'
```

4-email-secret.yaml
Replace placeholders with your actual email SMTP configuration.

```bash
apiVersion: v1
kind: Secret
metadata:
  name: alertmanager-email
  namespace: monitoring
type: Opaque
stringData:
  smtp-host: 'SMTP_SERVER_HOST'
  smtp-port: 'SMTP_SERVER_PORT'
  smtp-username: 'YOUR_EMAIL_USERNAME'
  smtp-password: 'YOUR_EMAIL_PASSWORD'
  from: 'YOUR_SENDER_EMAIL_ADDRESS'
  to: 'YOUR_RECEIVER_EMAIL_ADDRESS'
```

## Step 4: Configure Alertmanager
Update your Alertmanager configuration to include the Slack and email receivers and routing rules.

4-alert-manager-configuration.yaml
This configuration splits alerts based on labels to either Slack or email.

```bash
apiVersion: monitoring.coreos.com/v1
kind: AlertmanagerConfig
metadata:
  name: alertmanager-config
  namespace: monitoring
spec:
  receivers:
    - name: 'java-mysql-slack'
      slackConfigs:
        - apiURL:
            name: alertmanager-slack
            key: slack-webhook-url
          channel: '#YOUR_SLACK_CHANNEL'
          sendResolved: true
    - name: 'nginx-k8s-email'
      emailConfigs:
        - to: 'YOUR_RECEIVER_EMAIL_ADDRESS'
          from: 'YOUR_SENDER_EMAIL_ADDRESS'
          smarthost: 'SMTP_SERVER_HOST:SMTP_SERVER_PORT'
          authUsername:
            name: alertmanager-email
            key: smtp-username
          authPassword:
            name: alertmanager-email
            key: smtp-password
          sendResolved: true
  route:
    groupBy: ['alertname']
    groupWait: 30s
    groupInterval: 5m
    repeatInterval: 12h
    routes:
      - match:
          app: java|mysql
        receiver: java-mysql-slack
      - match:
          component: nginx|k8s
        receiver: nginx-k8s-email
```

## Step 5: Apply the Configuration
Apply the secrets and Alertmanager configuration to your cluster:

```bash
kubectl apply -f kubernetes-manifests/4-email-secret.yaml
kubectl apply -f kubernetes-manifests/4-slack-secret.yaml
kubectl apply -f kubernetes-manifests/4-alert-manager-configuration.yaml
```

Verification
Verify Secrets: Ensure the secrets are correctly applied by running kubectl get secrets -n monitoring.
Access Alertmanager UI: Use port-forwarding to access the Alertmanager UI and verify your configuration is loaded.
Test Notifications: Trigger test alerts (if possible) or wait for the conditions to naturally occur to ensure notifications are sent correctly.
This setup will help you automatically notify the responsible team members when specific issues arise, enhancing your response time and potentially preventing further complications.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/monitoring-with-prometheus/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
