To configure alert rules for your applications and Kubernetes components using Prometheus within the kube-prometheus-stack, you'll need to define PrometheusRule resources. These resources specify the conditions under which alerts should be triggered. Below are examples of how to set up alert rules for Nginx, MySQL, the Java application, and a Kubernetes StatefulSet representing your MySQL deployment.

## Step 1: Define Alert Rules
Create YAML files for each set of alert rules you want to configure. Each file will contain a PrometheusRule resource with the specified rules.

Nginx Ingress Alert Rule (3-nginx-alert-rules.yaml)

```bash
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: nginx-ingress-alerts
  namespace: monitoring
  labels:
    release: monitoring-stack
spec:
  groups:
  - name: nginx-ingress
    rules:
    - alert: HighHttpRequestErrorRate
      expr: sum(rate(nginx_ingress_controller_requests{status=~"4.."}[5m])) by (instance) / sum(rate(nginx_ingress_controller_requests[5m])) by (instance) > 0.05
      for: 1m
      labels:
        severity: critical
      annotations:
        summary: High HTTP request error rate detected
        description: More than 5% of HTTP requests have 4xx status codes.
```

MySQL Alert Rules (3-mysql-alert-rules.yaml)

```bash
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: mysql-alerts
  namespace: monitoring
  labels:
    release: monitoring-stack
spec:
  groups:
  - name: mysql
    rules:
    - alert: MySQLDown
      expr: mysql_up == 0
      for: 5m
      labels:
        severity: critical
      annotations:
        summary: All MySQL instances are down
        description: This alert fires if all MySQL instances are down.
    - alert: MySQLTooManyConnections
      expr: mysql_global_status_threads_connected > 100
      for: 5m
      labels:
        severity: warning
      annotations:
        summary: MySQL has too many connections
        description: This alert fires if MySQL has too many connections.
```

Java Application Alert Rule (3-java-alert-rules.yaml)

```bash
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: java-application-alerts
  namespace: monitoring
  labels:
    release: monitoring-stack
spec:
  groups:
  - name: java-application
    rules:
    - alert: HighRequestRate
      expr: sum(rate(http_requests_total{job="java-app"}[5m])) > 100
      for: 5m
      labels:
        severity: warning
      annotations:
        summary: Java application high request rate
        description: This alert fires if the Java application's request rate is too high.
```

StatefulSet Replicas Mismatch Alert Rule (3-k8s-alert-rules.yaml)

```bash
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: statefulset-replicas-mismatch
  namespace: monitoring
  labels:
    release: monitoring-stack
spec:
  groups:
  - name: statefulset
    rules:
    - alert: StatefulSetReplicasMismatch
      expr: kube_statefulset_replicas{job="kube-state-metrics"} != kube_statefulset_status_replicas_ready{job="kube-state-metrics"}
      for: 10m
      labels:
        severity: warning
      annotations:
        summary: StatefulSet replicas mismatch
        description: This alert fires if the desired number of replicas for a StatefulSet does not match the number of ready replicas.
```

## Step 2: Apply the Alert Rules
Apply each YAML file to your cluster:

```bash
kubectl apply -f kubernetes-manifests/3-nginx-alert-rules.yaml
kubectl apply -f kubernetes-manifests/3-mysql-alert-rules.yaml
kubectl apply -f kubernetes-manifests/3-java-alert-rules.yaml
kubectl apply -f kubernetes-manifests/3-k8s-alert-rules.yaml
```

## Verification and Adjustments
- Verify Alert Rules: Access the Prometheus UI and navigate to the Alerts section to verify your alert rules have been loaded correctly.
- Adjust Alert Expressions: Based on your environment and metrics, you may need to adjust the expressions to better fit your application's behavior and traffic patterns.
- Alertmanager Configuration: Ensure Alertmanager is configured to send notifications based on these alerts. You might need to configure routes, receivers, and integrations (e.g., email, Slack

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/monitoring-with-prometheus/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
